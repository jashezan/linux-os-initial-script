#!/bin/bash

# Description: Install updates and upgrading the system
echo -e "Install updates and upgrading the system"
sudo apt-get update
sudo apt-get -y upgrade

# Installing some basic tools
echo -e "Installing curl git python3 python3-pip wget zsh build-essential apt-transport gcc gdb g++";
sudo apt-get install -y curl
sudo apt-get install -y git
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
sudo apt-get install -y wget
sudo apt-get install -y zsh
sudo apt-get install -y build-essential
sudo apt-get install -y apt-transport
sudo apt-get install -y gcc
sudo apt-get install -y gdb
sudo apt-get install -y g++
sudo apt-get install -y unrar
sudo apt-get install -y rar
sudo apt-get install -y gnome-keyring
sudo apt-get install -y unzip
sudo apt-get install -y zip
sudo apt-get install -y p7zip-full
sudo apt-get install -y p7zip-rar

# Adding Repository
sudo add-apt-repository ppa:ipinfo/ppa

# Echo
# Brave Browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
# Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list


# Handy TOOLS
echo -e "Installing Handy TOOLS: fzf whois htop btop neofetch ipinfo"
sudo apt-get install -y fzf
sudo apt-get install -y whois
sudo apt-get install -y htop
sudo apt-get install -y btop
sudo apt install -y neofetch
sudo apt install -y ipinfo

# Installing Software
echo -e "Installing Software"
sudo apt install -y bleachbit


# VSCode
mkdir -p ~/Downloads/softwares
curl -L https://code.visualstudio.com/sha/download\?build\=stable\&os\=linux-deb-x64 -o ~/Downloads/softwares/vscode.deb
sudo dpkg -i ~/Downloads/softwares/vscode.deb
sudo apt-get install -f


sudo apt install brave-browser
sudo apt-get install sublime-text

# Setting UP KDE
mkdir -p ~/.kde/share/config

# Downloading font
mkdir -p ~/Downloads/fonts
curl https://gitlab.com/jashezan/linux-fonts/-/raw/main/bangla_fonts.zip -o ~/Downloads/fonts/banglafont.zip
curl https://gitlab.com/jashezan/linux-fonts/-/raw/main/KaliLinuxin-Fonts.zip -o ~/Downloads/fonts/kalilinuxfont.zip
sudo unzip ~/Downloads/fonts/banglafont.zip -d /usr/share/fonts
sudo unzip ~/Downloads/fonts/kalilinuxfont.zip -d /usr/share/fonts

mkdir -p ~/.config/sublime-text/Packages/User/
cat ~/.config/sublime-text/Packages/User/Preferences.sublime-settings

# Removing Games and Thunderbird
sudo apt-get remove --purge kmahjongg kmines ksudoku kpat && sudo apt-get --auto-purge
sudo apt-get remove --purge thunderbird
